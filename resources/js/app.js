require('./bootstrap');
window.Vue = require('vue');

Vue.component('table-component', require('./components/pages/proveedor-externo/table.vue').default);
Vue.component('new-component', require('./components/pages/proveedor-externo/new.vue').default);

const app = new Vue({
    el: '#app',
});
