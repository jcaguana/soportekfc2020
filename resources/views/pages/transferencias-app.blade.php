@extends('../layouts.app')

@section('content')
    <main class="content">
        <div class="container-fluid p-0">


            <h1 class="h3 mb-3">Transferencias app móvil<strong> Soporte KFC</strong></h1>

            <div class="row">


                <div class="col-12 col-lg-12">
                    <div class="card">
                        <div class="card-body">



                            <div class="card-header">
                                        <h6 class="card-title">App móvil KFC</h6>

                                        <div class="card-search-table">

                                                <h6 class="card-subtitle text-muted">Transferencias pedidos domicilio app móvil KFC</h6>
                                                <div class="input-group input-group-navbar card-search-table-left">
                                                    <input type="date" class="form-control" placeholder="Buscar" aria-label="Search">
                                                </div>
                                                <div class="card-search-table-left">
                                                    <button class="btn btn-pill btn-secondary" id="Buscar">Buscar</button>
                                                    <button class="btn btn-pill btn-success" id="Actualizar">Recargar</button>
                                                    <button class="btn btn-pill btn-primary" id="Actualizar">Validar</button>
                                                  </div>
                                            </div>
                                    </div>

                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th style="width:5%;">Tienda</th>
                                            <th style="width:15%">Cod.Factura</th>
                                            <th style="width:10%">Fecha</th>
                                            <th style="width:25%">Cod.AppMovil</th>
                                            <th style="width:10%">F.Pago</th>
                                            <th style="width:10%">Total</th>
                                            <th style="width:5%">Medio</th>
                                            <th style="width:5%">Estado</th>
                                            <th style="width:5%">Transferencias</th>
                                            <th style="width:5%">Soporte</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>K008</td>
                                            <td>K008F000227163</td>
                                            <td>06/06/2020</td>
                                            <td>0000900335-0101</td>
                                            <td>VISA</td>
                                            <td>27,39</td>
                                            <td>WEB</td>
                                            <td>Entregado</td>
                                            <td>K044</td>
                                            <td><p class="text-success">Transferido</p></td>
                                        </tr>
                                        <tr>
                                            <td>K008</td>
                                            <td>K008F000227163</td>
                                            <td>06/06/2020</td>
                                            <td>0000900335-0101</td>
                                            <td>VISA</td>
                                            <td>27,39</td>
                                            <td>APP</td>
                                            <td>Entregado</td>
                                            <td>K044</td>
                                            <td><p class="text-success">Transferido</p></td>
                                        </tr>
                                        <tr>
                                            <td>K008</td>
                                            <td>K008F000227163</td>
                                            <td>06/06/2020</td>
                                            <td>0000900335-0101</td>
                                            <td>VISA</td>
                                            <td>27,39</td>
                                            <td>WEB</td>
                                            <td>Entregado</td>
                                            <td>K044</td>
                                            <td><p class="text-success">Transferido</p></td>
                                        </tr>
                                        <tr>
                                            <td>K008</td>
                                            <td>K008F000227163</td>
                                            <td>06/06/2020</td>
                                            <td>0000900335-0101</td>
                                            <td>VISA</td>
                                            <td>27,39</td>
                                            <td>APP</td>
                                            <td>Entregado</td>
                                            <td>K044</td>
                                            <td><p class="text-secondary">Pendiente</p></td>
                                        </tr>
                                        </tbody>
                                    </table>


                        </div>
                    </div>
                </div>
            </div>


        </div>
    </main>
@endsection
