@extends('../layouts.app')

@section('content')







    <main class="content">
        <div class="container-fluid p-0">

            <h1 class="h3 mb-3">Proveedores Externos<strong> Soporte KFC</strong></h1>


            <div class="row">

                <div class="col-12 col-lg-12">



                    <ul class="nav nav-pills card-header-pills pull-right" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tab-1">Listar</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab-2">Buscar</a>
                        </li>
                    </ul>


                    <div class="card-body">
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="tab-1" role="tabpanel">
                                    <div class="row">
                                        <div class="card col-12">
                                            <div class="card-body">

                                                <table-component></table-component>
												
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="tab-2" role="tabpanel">

                                        <new-component></new-component>

                                 </div>
                            </div>
                        </div>


                </div>
            </div>


        </div>
    </main>


@endsection
