<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}" />
    <title>{{ 'Transferencias App móvil KFC' }}</title>

</head>
<style>
    html,body {
        font-family: sans-serif;

    }
    .table {
        width: 100%;
      }

    .table th,
    .table td {
        padding: 0.75rem;
        vertical-align: top;
     }

    .table thead th {

        border-bottom: 2px solid #dee2e6!important;
    }

    .table tbody td {
        vertical-align: bottom;
        border-bottom: 2px solid #dee2e6;
    }



    .col {
        flex-basis: 0;
        flex-grow: 1;
        min-width: 0;
        max-width: 100%;
    }
</style>
<body>
    <div style="width: 100%;padding-right: 15px;padding-left: 15px;margin-right: auto;margin-left: auto;">
        <div style="display: flex;flex-wrap: wrap;margin-right: -15px;margin-left: 0px;">
            <div  style="width: auto">
                <div style="position: relative;display: flex;flex-direction: column;min-width: 0;  word-wrap: break-word;background-color: #fff;background-clip: border-box;border: 1px solid rgba(0, 0, 0, 0.125);border-radius: 0.25rem;">
                    <div style="flex: 1 1 auto;min-height: 1px;padding: 1.25rem;">
                        <div style="padding: 0.75rem 1.25rem;margin-bottom: 0;background-color: rgba(0, 0, 0, 0.03);border-bottom: 1px solid rgba(0, 0, 0, 0.125);">

                        <h6 style="font-size: 15px">Transferencias App móvil KFC - TEST JOB</h6>
                         <p><small>( {{count($data)}} ) Transferencias pedidos domicilio app móvil KFC del día {{ date('d/m/Y') }} de la ultima hora</small></p>

                        </div>

                        @if( count($data) > 0 )
                        <table class="table table-striped" >
                            <thead>
                            <tr>
                                <th scope="col">Tienda</th>
                                <th scope="col">F.Pago</th>
                                <th scope="col">Hora</th>
                                <th scope="col">Total</th>
                                <th scope="col">Referencia</th>
                                <th scope="col">Id_Adquirente</th>
                            </tr>
                            </thead>
                            <tbody style="text-align: center">

                            @foreach($data as $item)
                            <tr>
                                <td scope="col" > {{ $item->tienda }} </td>
                                <td scope="col" > {{ $item->forma_pago }} </td>
                                <td scope="col" > {{ $item->Hora }} </td>
                                <td scope="col" > {{ $item->total }} </td>
                                <td scope="col" > {{ $item->referencia }} </td>
                                <td scope="col" > {{ $item->Id_Adquirente }} </td>
                            </tr>
                            @endforeach
                              </tbody>
                        </table>
                            @else
                            <p>No se realizaron transferencias en la ultima hora.</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
</body>
</html>
