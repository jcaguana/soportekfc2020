@extends('layouts.app')

@section('content')

    <div class="wrapper">
        <div class="main">


            <main class="content">
                <div class="container-fluid p-0">
                    <div class="row" style="width: 350px;margin-left: auto; margin-right: auto;margin-top: 5%">
                        <div class="col-12 col-xl-12">
                                <div class="text-center">
                                    <h5 class="card-title">Iniciar Sesión</h5>
                                 </div>
                                <div class="card-body">
                                    <form method="POST" action="{{ route('login') }}">
                                        @csrf
                                        <div class="form-group">
                                            <label class="form-label">Correo electrónico</label>
                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  autocomplete="email" autofocus placeholder="nombre.apellido@kfc.com.ec" >

                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Contraseña</label>
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="current-password" placeholder="•••••••••">

                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
	
	                                 <div class="form-group">
											<div class="col-12">
												<span class="d-block pt-2 mt-4 border-top">¿Olvidaste tu <a href="/password/reset">contraseña?</a></span>
											</div>
                                    </div>
									
                                        <button type="submit" class="btn btn-primary btn-block">Iniciar Sesión</button>
                                    </form>
                                </div>
                        
						</div>
                    </div>
                </div>
            </main>

        </div>
    </div>

@endsection
