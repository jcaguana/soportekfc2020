<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}" />

    <title>{{ config('app.name', 'Soporte KFC') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <script src="https://unpkg.com/feather-icons"></script>
    <script src="https://cdn.jsdelivr.net/npm/feather-icons/dist/feather.min.js"></script>
 
	<!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body>

<style>

	.atom-spinner{
		margin-left: auto!important;
		margin-right: auto!important;
	}
	
	</style>

@guest

<style>
    .btn-primary{
        background-color: #a40c25;
        border-color: #a40c25;
    }
    .btn-primary:hover{
        background-color: #a40c25;
        border-color: #a40c25;
    }

</style>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Soporte KFC') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        <main >
            @yield('content')
        </main>
    </div>
@else
    <link href="{{ asset('css/template.css') }}" rel="stylesheet">

    <style>
        .sidebar-header{
            color: #222e3c!important;
            font-weight: bold;
        }
        .sidebar-link, a.sidebar-link{
         background-color: #ffffff!important;
         color: #222e3c!important;

        }
    </style>
    <div id="app" class="wrapper">
        <nav id="sidebar" class="sidebar   navbar-expand-md navbar-light bg-white shadow-sm">
            <ul class="sidebar-nav">
                <li class="sidebar-header" >
                    <a href="/home"  >
                        <img src="{{ asset('favicon.png') }}" alt="Soporte KFC" style="width: 120px;">
                    </a>

                </li>

                <li class="sidebar-header">
                    Mi Cuenta
                </li>

                <li class="sidebar-item">
                    <a href="#auth" data-toggle="collapse" class="sidebar-link collapsed">
                        <i class="align-middle" data-feather="briefcase"></i> <span class="align-middle">{{ Auth::user()->name }}</span>
                    </a>
                    <ul id="auth" class="sidebar-dropdown list-unstyled collapse " data-parent="#sidebar">

                            <a  class="sidebar-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                {{ __('Cerrar sesión') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </li>


                <li class="sidebar-header">
                    Tools & Components
                </li>

                <li class="sidebar-item">
                    <a href="#master-data" data-toggle="collapse" class="sidebar-link collapsed">
                        <i class="align-middle" data-feather="briefcase"></i> <span class="align-middle">Master Data</span>
                    </a>

                    <ul id="master-data" class="sidebar-dropdown list-unstyled collapse " data-parent="#sidebar">
                        <li class="sidebar-item"><a class="sidebar-link" href="/proveedores-externos">Proveedor Externo</a></li>
                    </ul>
                </li>
                <li class="sidebar-item">
                    <a href="#auditoria" data-toggle="collapse" class="sidebar-link collapsed">
                        <i class="align-middle" data-feather="briefcase"></i> <span class="align-middle">Auditoría</span>
                    </a>

                    <ul id="auditoria" class="sidebar-dropdown list-unstyled collapse " data-parent="#sidebar">
                        <li class="sidebar-item"><a class="sidebar-link" href="/transferencias-app">Transferencias APP</a></li>
                    </ul>
                </li>

            </ul>


        </nav>

        <div class="main">
            @yield('content')
        </div>
    </div>
@endguest



</body>



</html>
