<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');


Route::get('/email', function(){

    $data = [];
    //return view('pages.email.email',compact('data'));
    //\App\Jobs\SendEmail::dispatch();

});













Route::get('proveedores-externos',                'ProveedorExternoController@view')->middleware('auth');
Route::get('transferencias-app',                'TransferenciasApp@view')->middleware('auth');

