<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::get('cadenas',                     			'CadenaController@index');
Route::get('restaurantes/{cdn_id}/{documento}',       'RestauranteController@show');



Route::group(['prefix' => 'masterdata'], function ( ) {
	
	Route::group(['prefix' => 'proveedores-externos'], function ( ) {
		Route::get('/',                'ProveedorExternoController@index');
		Route::get('/{documento}/',    'ProveedorExternoController@show');
		Route::post('actualizar',      'ClienteController@store');
		Route::post('nuevo',           'ProveedorExternoController@store');
	});

		
});
