<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestauranteDistribuidor extends Model
{
    protected $table = "Restaurante";

    protected $fillable = [
        'rst_id', 
		'ciu_id', 
		'rst_cod_tienda', 
		'rst_descripcion', 
		'rst_direccion', 
		'rst_fono', 
		'TelefonoDomiclio',
		'rst_localizacion',
    ];
		  
}
