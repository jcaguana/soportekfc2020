<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class SoporteKFC extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $data =  DB::connection('srvv-zeus')
            ->select( DB::raw("select  REPLACE(Merchantid, '000000kfc', 'K')  as tienda
            ,Id_Grupo_Tarjeta					                                    as forma_pago
            ,SUBSTRING(Hora_Transaccion ,1,2)+':'+SUBSTRING(Hora_Transaccion ,3,2)+':'+SUBSTRING(Hora_Transaccion ,5,2)			as Hora
            ,Face_Value                                                             as total
            ,Numero_Referencia                                                      as referencia
            ,Id_Adquirente                                                          as Id_Adquirente
            from ST_Transaccional
            where convert(varchar(15) , Fecha_Transaccion, 103) = convert(varchar(15) , getdate() , 103)
            and Merchantid like '%000000kfc%'
            and SUBSTRING(Hora_Transaccion ,1,2) = DATEPART(HOUR, GETDATE())-1
            and Id_Adquirente <> 'MEDIANET'
            order by Hora_Transaccion desc
          "));



        return $this->view('pages.email.email',compact('data'));
    }
}
