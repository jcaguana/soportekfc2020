<?php

namespace App\Jobs;

use App\Mail\SoporteKFC;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $users = [
            "jcaguana@netconsulting.com.ec"
        ];



        Mail::to("jeisoncaguana@gmail.com")->cc( $users)->send(new SoporteKFC());
        Log::info("Enviando correo del hora ".date('Y/m/d h:i:s') );



    }
}
