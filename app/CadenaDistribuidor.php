<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CadenaDistribuidor extends Model
{
    protected $table = "Cadena";

    protected $fillable = [
        'cdn_id', 
		'cdn_descripcion', 
		'cdn_logotipo', 
    ];	  
}
