<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Auditoria extends Model
{


    protected $table = "auditoria";

    protected $fillable = [
        'user_id', 'description', 'page','action'
    ];

}
