<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClienteMasterData extends Model
{
    protected $table = "Cliente";

    protected $fillable = [
        'Nombre', 
		'Apellido', 
		'Descripcion', 
		'TipoIdentificacion', 
		'Identificacion', 
		'DireccionDomicilio', 
		'TelefonoDomiclio',
		'Correo',
		'FechaUltimaActualizacion',
		'TipoCliente',
    ];
	
}
