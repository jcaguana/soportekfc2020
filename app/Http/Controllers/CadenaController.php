<?php

namespace App\Http\Controllers;

use App\CadenaDistribuidor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CadenaController extends Controller
{
    
	public function index()
    {
		
	
	 $data  =  DB::connection('sazmaxmanager')
				->table('Cadena')
				->where('cdn_id','<>', 0 )
				->orderBy('cdn_descripcion','asc')
				->get();
				
				
				
		 if ( count( $data)  == 0) {
            return response()->json([], 204);
        }else {
            return response()->json([
                    "message" => "Lista de cadenas ",
                    "data" => $data,
                    "errors" => []
            ], 200);
        }
		
	}
}
