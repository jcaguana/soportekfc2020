<?php

namespace App\Http\Controllers;

use App\ClienteMasterData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class ClienteController extends Controller
{

    public function index()
    {
        //
    }


    public function create()
    {

    }

    public function store(Request $request)
    {
        $validacion = Validator::make($request->all(), [
 			'Descripcion' => 'required|max:255',
 			'TipoIdentificacion' => 'required|max:255',
 			'Identificacion' => 'required|max:50',
 			'DireccionDomicilio' => 'required|max:255',
 			'TelefonoDomiclio' => 'required|max:255',
            'Correo' => 'required|email|max:255'
        ]);

        if ($validacion->fails()) {
            return response()->json([
                'error'=>$validacion->errors()
            ], 401);
        }

		$input = $request->all();
        $input['FechaUltimaActualizacion']  = date("d-m-Y H:i:s");
        $input['TipoCliente'] 				= 'EXTERNO';




    /// inicia proceso en master data


        // busca si el cliente existe en master data
        $cliente_azure = DB::connection('srvv-zeus')
            ->table('Cliente')
            ->where('Identificacion','=',$input['Identificacion'] )
            ->get();

        // si no existe crea el cliente  en master data
        if ( count($cliente_azure) == 0  ){

             $insert_azure = DB::connection('srvv-zeus')
                 ->insert('insert into Cliente (	
				Descripcion, TipoIdentificacion, Identificacion, DireccionDomicilio, TelefonoDomiclio, Correo, FechaUltimaActualizacion, TipoCliente) 			
				values (?,?,?,?,?,?,?,?)', [
                $input['Descripcion']
                ,$input['TipoIdentificacion']
                ,$input['Identificacion']
                ,$input['DireccionDomicilio']
                ,$input['TelefonoDomiclio']
                ,$input['Correo']
                ,$input['FechaUltimaActualizacion']
                ,$input['TipoCliente']
            ]);





                //si no inserta muestra retorna
                if ( !$insert_azure ) {
                    return response()->json([
                        "message" => "No fue posible insertar la información en Master Data",
                        "data" => [],
                        "errors" => []
                    ], 200);
                }


        }else {
            // si  existe modifica el cliente  en master data

            $update = DB::connection('srvv-zeus')
                ->table('Cliente')
                ->where('Identificacion','=',$input['Identificacion'] )
                ->update($input);

            //si no modifica  muestra retorna

            if (  !$update  ) {
                return response()->json([
                    "message" => "No fue posible modificar la información en Master Data",
                    "data" => [],
                    "errors" => []
                ], 200);

            }
        }






        /// inicia proceso en el centralizador


        //busca el IDTipoDocumento en el centralizador
		$IDTipoDocumento = DB::connection('sazmaxmanager')
		->table('Tipo_Documento')
		->where('tpdoc_descripcion','=', $input['TipoIdentificacion'] )
		->select('IDTipoDocumento')
		->get();


       //si no encuentra IDTipoDocumento retorna el mensaje
        if ( count ( $IDTipoDocumento ) == 0) {
			return response()->json([
						"message" => "No fue posible modificar, el tipoTipo_Documento es incorrecto o no existe ",
						"data" => [],
						"errors" => []
				], 200);
		}



        //busca el UsrModifica en el centralizador
		$UsrModifica = DB::connection('sazmaxmanager')
		->table('Users_Pos')
		->where('usr_usuario','=','amaxpoint' )
		->select('IDUsersPos')
		->get();

        //si no encuentra  el UsrModifica retorna el mensaje
        if ( count ( $UsrModifica ) == 0) {
			return response()->json([
						"message" => "No fue posible modificar, el Users_Pos es incorrecto o no existe ",
						"data" => [],
						"errors" => []
				], 200);
		}





        // busca si el cliente existe en centralizador
        $cliente_distribuidor = DB::connection('sazmaxmanager')
            ->table('Cliente')
            ->where('cli_documento','=',$input['Identificacion'] )
            ->get();



        // si no existe el cliente en el centralizador inserta
        if ( count( $cliente_distribuidor) == 0) {
            //insert
            $insert_distribuidor = DB::connection('sazmaxmanager')
                ->insert('insert into Cliente (	
				cli_nombres, cli_apellidos, IDTipoDocumento, cli_documento, cli_direccion, cli_telefono, cli_email, cli_tipo_cliente, UsrModifica,FechaCreacion, FechaActualizacion) 			
				values (?,?,?,?,?,?,?,?,?,?,?)', [
                    $input['Descripcion']
                    ,''
                    ,$IDTipoDocumento[0]->IDTipoDocumento
                    ,$input['Identificacion']
                    ,$input['DireccionDomicilio']
                    ,$input['TelefonoDomiclio']
                    ,$input['Correo']
                    ,'EXT'
                    ,$UsrModifica[0]->IDUsersPos
                    ,date("Y-m-d H:i:s")
                    ,date("Y-m-d H:i:s")
                ]);

            if ( !$insert_distribuidor ) {

                return response()->json([
                    "message" => "No fue posible insertar la información en Distribuidor",
                    "data" => [],
                    "errors" => []
                ], 200);
            }


        }else {
            //si existe el cliente en el centralizador, modifica
            $update_distribuidor = DB::connection('sazmaxmanager')
                ->table('Cliente')
                ->where('cli_documento','=',$input['Identificacion'])
                ->update([
                    "cli_nombres" => $input['Descripcion'],
                    "cli_apellidos" => "",
                    "IDTipoDocumento" => $IDTipoDocumento[0]->IDTipoDocumento,
                    "cli_documento" => $input['Identificacion'],
                    "cli_direccion" => $input['DireccionDomicilio'],
                    "cli_telefono" => $input['TelefonoDomiclio'],
                    "cli_email" => $input['Correo'],
                    "cli_tipo_cliente" => 'EXT',
                    "UsrModifica" => $UsrModifica[0]->IDUsersPos,
                    "FechaActualizacion" => date("Y-m-d H:i:s")

                ]);

            if ( !$update_distribuidor ) {
                return response()->json([
                    "message" => "Me modificó la información correctamente",
                    "data" => $input,
                    "errors" => []
                ], 200);
            }

        }

		return response()->json([
				"message" => "Me modificó la información correctamente",
				"data" => $input,
				"errors" => []
		], 200);

	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
