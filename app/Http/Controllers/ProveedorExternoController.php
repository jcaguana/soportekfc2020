<?php

namespace App\Http\Controllers;

use App\ClienteMasterData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class ProveedorExternoController extends Controller
{

    public function __construct()
    {

    }

    public function view () {
        return view('pages/proveedores-externos');
    }

    public function index()
    {
		//segunda ----!master data
       // $srvv_zeus     = DB::connection('srvv-zeus')->table('Cliente')->get();

	   // tercera ---
	   //$sazmaxmanager  = DB::connection('sazmaxmanager')->table('Cliente')->get();



         $data  =  DB::connection('srvv-zeus')
				->table('Cliente')
				->where('Identificacion','<>','')
				->where('Identificacion','<>',null)
				->where('Nombre','<>','')
				->where('Nombre','<>',null)
				->where('Apellido','<>','')
				->where('Apellido','<>',null)
				->where('DireccionDomicilio','<>','')
				->where('DireccionDomicilio','<>',null)
				->where('Correo','<>','')
				->where('Correo','<>',null)
				->orderBy('Nombre','asc')
				->orderBy('Apellido','asc')
				->paginate(10);



        if ( count( $data)  == 0) {
            return response()->json([], 204);
        }else {
            return response()->json([
                    "message" => "Lista de proveedores externos de master data",
                    "data" => $data,
                    "errors" => []
            ], 200);
        }

    }

    public function create()
    {
        //
    }


    public function store(Request $request)
    {

		$validacion = Validator::make($request->all(), [
 			'Descripcion' => 'required|max:255',
  			'Identificacion' => 'required|max:50',
 			'DireccionDomicilio' => 'required|max:255',
 			'TelefonoDomiclio' => 'required|max:255',
            'Correo' => 'required|email|max:255'
        ]);

        if ($validacion->fails()) {
            return response()->json([
                'error'=>$validacion->errors()
            ], 401);
        }

		$input = $request->all();
        $input['FechaUltimaActualizacion']  = date("d-m-Y H:i:s");
        $input['TipoCliente'] 				= 'EXTERNO';
        $input['TipoIdentificacion'] 		= 'CEDULA';


			$IDTipoDocumento = DB::connection('sazmaxmanager')
		->table('Tipo_Documento')
		->where('tpdoc_descripcion','=', $input['TipoIdentificacion'] )
		->select('IDTipoDocumento')
		->get();

		if ( count ( $IDTipoDocumento ) == 0) {
			return response()->json([
						"message" => "No fue posible modificar, el tipoTipo_Documento es incorrecto o no existe ",
						"data" => [],
						"errors" => []
				], 200);
		}


		$UsrModifica = DB::connection('sazmaxmanager')
		->table('Users_Pos')
		->where('usr_usuario','=','amaxpoint' )
		->select('IDUsersPos')
		->get();


		if ( count ( $UsrModifica ) == 0) {
			return response()->json([
						"message" => "No fue posible modificar, el Users_Pos es incorrecto o no existe ",
						"data" => [],
						"errors" => []
				], 200);
		}






		$insert_distribuidor = DB::connection('sazmaxmanager')
				->insert('insert into Cliente (	
				cli_nombres, cli_apellidos, IDTipoDocumento, cli_documento, cli_direccion, cli_telefono, cli_email, cli_tipo_cliente, UsrModifica,FechaCreacion, FechaActualizacion) 			
				values (?,?,?,?,?,?,?,?,?,?,?)', [
					 $input['Descripcion']
					 ,''
					 ,$IDTipoDocumento[0]->IDTipoDocumento
					 ,$input['Identificacion']
					 ,$input['DireccionDomicilio']
					 ,$input['TelefonoDomiclio']
					 ,$input['Correo']
					 ,'EXT'
					 ,$UsrModifica[0]->IDUsersPos
					 ,date("Y-m-d H:i:s")
					 ,date("Y-m-d H:i:s")
				]);



			if ( !$insert_distribuidor ) {

				return response()->json([
						"message" => "No fue posible insertar la información en Distribuidor",
						"data" => [],
						"errors" => []
				], 200);
			}

		return response()->json([
				"message" => "Me inserto la información correctamente",
				"data" => $input,
				"errors" => []
		], 200);




    }


    public function show( $documento )
    {
		$data = DB::connection('srvv-zeus')
		->table('Cliente')
		->where('Identificacion','=', $documento )
 		->get();

		if ( count( $data ) == 0  ) {
			return response()->json([
					"message" => "No existe infromación para mostrar",
					"data" => [],
					"errors" => []
			], 200);
		}

		return response()->json([
				"message" => "Información de ".$documento,
				"data" => $data,
				"errors" => []
		], 200);


     }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
