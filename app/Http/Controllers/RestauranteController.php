<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
 
class RestauranteController extends Controller
{
    public function show( $cdn_id , $documento )
    { 	 

		 
								
		$data = DB::connection('sazmaxmanager')
			->table( 'Restaurante' )
			->where('cdn_id','=', $cdn_id )
			->orderBy('rst_cod_tienda','asc')
			->get();
						
				
				
		 if ( count( $data)  == 0) {
            return response()->json([], 204);
        }else {
            return response()->json([
                    "message" => "Lista de Restaurantes ",
                    "data" => $data,
                    "errors" => []
            ], 200);
        }
							
							 
		 /*
		 die();
	 $statement = DB::connection('sazmaxmanager')
	 ->select( DB::raw("
						--inicio 
							DECLARE @cdn_id INT = ".$cdn_id.", 
							@documento VARCHAR(50) = '".$documento."';
                            
DECLARE @tb_Serivor_tienda TABLE  ( 
                                        id INT IDENTITY (1,1)
                                        , rst_id INT NOT NULL
                                        , rst_cod_tienda VARCHAR(6) NOT NULL
                                        , servidor VARCHAR(50) NOT NULL
                                    ) 

                                    INSERT INTO @tb_Serivor_tienda  
                                    SELECT  r.rst_id                                            AS rst_id
                                            ,r.rst_cod_tienda                                   AS rst_cod_tienda
                                            ,'['+ip+'\'+Instancia+'].' + Databasename           AS servidor
                                    FROM    BasesReplicacion    AS b
                                            INNER JOIN Restaurante      AS r    ON b.rst_id   = r.rst_id  AND r.cdn_id = @cdn_id 
                                    ORDER BY    r.rst_id ASC
                                    
                                     
                                    DECLARE  @rst_id INT
                                            ,@rst_cod_tienda VARCHAR(6)
                                            ,@Servidor VARCHAR(50)
                                            ,@consultaSQL VARCHAR(max)
                                            ,@totalregistos INT = (SELECT COUNT(rst_id) FROM @tb_Serivor_tienda )
                                            ,@id_contador INT = 1
                                    
                                         
									IF OBJECT_ID('tempdb..#temp') is not null
										BEGIN
											DROP TABLE #temp
										END 
										
                                    CREATE TABLE #temp  (
                                        rst_id INT NOT NULL
                                        , IDTipoDocumento VARCHAR(MAX) NULL
                                        , cli_documento VARCHAR(50) NULL
                                    )
                                    
                                    WHILE ( @id_contador  <= @totalregistos )
                                    BEGIN 
                                        SELECT  @rst_id             = rst_id
                                                ,@rst_cod_tienda    = rst_cod_tienda
                                                ,@Servidor          = Servidor
                                        FROM    @tb_Serivor_tienda  
                                        WHERE   id = @id_contador
                                          
                                          SET @consultaSQL = 'SELECT '+CAST( @rst_id AS VARCHAR(4))+' AS rst_id
															, IDTipoDocumento
                                                            , cli_documento
                                                            FROM ( SELECT * FROM '+@Servidor+'.dbo.Cliente WHERE cli_documento = '''+@documento+''' ) AS tem ';
                                            BEGIN TRY  
                                                INSERT INTO #temp 
                                                EXEC  ( @consultaSQL );
                                            END TRY  
                                            BEGIN CATCH  
                                                PRINT 'Error: '+@rst_cod_tienda+': '+ERROR_MESSAGE()  
                                            END CATCH       
                                            SET @id_contador = @id_contador +1;
                                    END 
                                      
                                    SELECT  CASE
                                                WHEN tem.rst_id IS NULL THEN CAST('false' AS BIT)
                                                WHEN tem.rst_id IS NOT NULL THEN CAST('true' AS BIT)    
                                            END AS isCheckboks
                                            ,_r.rst_id
                                            ,_r.rst_cod_tienda
                                            ,_r.cdn_id
                                            ,_r.rst_descripcion 
                                            FROM #temp AS tem
                                            RIGHT JOIN dbo.Restaurante AS _r ON _r.rst_id = tem .rst_id 
                                            WHERE  _r.cdn_id = @cdn_id
                                            ORDER BY _r.rst_cod_tienda ASC 
									 
							
						--fin
						"));
						
					
 					return $statement;
 */
	 
	}
	
}
